Set-Up Instructions:

1. Sign into your email (which this folder has been sent to) on the phone
2. Double-tap on the attachment (hit use package installer)
2b. Your phone will likely say "downloads from this unknown source restricted" or something similar. 	Just click through it and hit allow from this source
3. Click through any other pop ups and then open the app


Once you click the blue "MoCap" button, the capture will start. Data is sent to the link below


Airtable Database: https://airtable.com/shrPuKWzXQRAq75mp  

For the markerless demo (app-release.apk), the coordinate data and angles are labeled using the corresponding human body parts.

For the marker demo (markerdemo.apk), because the system is only looking for locations that fall within the specified color ranges, the coordinates are labeled under POIs with an associated id number rather than body parts. Because of this, the angle data is labeled similarly, with Angle 1 representing the angle an invisible line that connects the 1st and 2nd points of interest is with the horizontal of the frame.


Note: Before each run, a line is made in the database marking the start