import 'package:flutter/material.dart';
import 'package:camera/camera.dart';
import 'package:tflite/tflite.dart';
import 'dart:math' as math;
import 'models.dart';
import 'package:dio/dio.dart';
import 'dart:math';

typedef void Callback(List<dynamic> list, int h, int w);

class Camera extends StatefulWidget {
  final List<CameraDescription> cameras;
  final Callback setRecognitions;
  final String model;

  Camera(this.cameras, this.model, this.setRecognitions);

  @override
  _CameraState createState() => new _CameraState();
}

class _CameraState extends State<Camera> {
  CameraController controller;
  bool isDetecting = false;

  @override
  void initState() {
    super.initState();

    if (widget.cameras == null || widget.cameras.length < 1) {
    } else {
      controller = new CameraController(
        widget.cameras[0],
        ResolutionPreset.low,
      );
      controller.initialize().then((_) {
        if (!mounted) {
          return;
        }
        setState(() {});

        controller.startImageStream((CameraImage img) {
          if (!isDetecting) {
            isDetecting = true;

            int startTime = new DateTime.now().millisecondsSinceEpoch;
            int width = (img.width * 0.25).toInt();
            int height = (img.height * 0.25).toInt();
            if (widget.model == mobilenet) {
              Tflite.runModelOnFrame(
                bytesList: img.planes.map((plane) {
                  return plane.bytes;
                }).toList(),
                imageHeight: height,
                imageWidth: width,
                numResults: 32,
              ).then((recognitions) {
                widget.setRecognitions(recognitions, img.height, img.width);

                isDetecting = false;
              });
            } else if (widget.model == posenet) {
              Tflite.runPoseNetOnFrame(
                bytesList: img.planes.map((plane) {
                  return plane.bytes;
                }).toList(),
                imageHeight: height,
                imageWidth: width,
                numResults: 32,
              ).then((recognitions) {
                int endTime = new DateTime.now().millisecondsSinceEpoch;
                print("Detection took ${endTime - startTime}");
                sendData(recognitions);
                widget.setRecognitions(recognitions, img.height, img.width);

                isDetecting = false;
              });
            } else {
              Tflite.detectObjectOnFrame(
                bytesList: img.planes.map((plane) {
                  return plane.bytes;
                }).toList(),
                model: widget.model == yolo ? "YOLO" : "SSDMobileNet",
                imageHeight: img.height,
                imageWidth: img.width,
                imageMean: widget.model == yolo ? 0 : 127.5,
                imageStd: widget.model == yolo ? 255.0 : 127.5,
                numResultsPerClass: 1,
                threshold: widget.model == yolo ? 0.2 : 0.4,
              ).then((recognitions) {
                int endTime = new DateTime.now().millisecondsSinceEpoch;
                print("Detection took ${endTime - startTime}");
                widget.setRecognitions(recognitions, img.height, img.width);
                isDetecting = false;
              });
            }
          }
        });
      });
    }
  }

  @override
  void dispose() {
    controller?.dispose();
    super.dispose();
  }
       Future<bool> sendData(inputData) async {
    if (inputData.toString().compareTo("[]") != 0)
    {
      try {
        //make angle calculations
        String calculationOutput = "";
        if (inputData.toString().contains("rightKnee")) {
          String rightKneeInfo = inputData.toString().substring(
              inputData.toString().indexOf("rightKnee") + 10,
              inputData.toString().indexOf("rightKnee") + 52);
          print("rKneeInfo: " + rightKneeInfo);
          var rKneeX = double.parse(rightKneeInfo.substring(
              rightKneeInfo.indexOf(":") + 1, rightKneeInfo.indexOf(",")));
          var rKneeY = double.parse(rightKneeInfo.substring(
              rightKneeInfo.indexOf(",") + 4, rightKneeInfo.length));
          print("rKneeX: " + rKneeX.toString());
          print("rKneeY: " + rKneeY.toString());

          String rightHipInfo = inputData.toString().substring(
              inputData.toString().indexOf("rightHip") + 10,
              inputData.toString().indexOf("rightHip") + 52);
          print("rHipInfo: " + rightHipInfo);
          var rHipX = double.parse(rightHipInfo.substring(
              rightHipInfo.indexOf(":") + 1, rightHipInfo.indexOf(",")));
          var rHipY = double.parse(rightHipInfo.substring(
              rightHipInfo.indexOf(",") + 4, rightHipInfo.length));
          print("rHipX: " + rHipX.toString());
          print("rHipY: " + rHipY.toString());

          var calc1X = rHipX - rKneeX;
          if (calc1X < 0) {
            calc1X = calc1X * -1;
          }

          var calc1Y = rHipY - rKneeY;
          if (calc1Y < 0) {
            calc1Y = calc1Y * -1;
          }
          var angle1 = atan(calc1Y / calc1X);
          calculationOutput = calculationOutput + "Angle1(RHip and RKnee): " +
              angle1.toString() + " ";
          print("Angle Calculation 1: " + angle1.toString());
        }

        if (inputData.toString().contains("leftKnee")) {
          String leftKneeInfo = inputData.toString().substring(
              inputData.toString().indexOf("leftKnee") + 10,
              inputData.toString().indexOf("leftKnee") + 52);
          print("lKneeInfo: " + leftKneeInfo);
          var lKneeX = double.parse(leftKneeInfo.substring(
              leftKneeInfo.indexOf(":") + 1, leftKneeInfo.indexOf(",")));
          var lKneeY = double.parse(leftKneeInfo.substring(
              leftKneeInfo.indexOf(",") + 4, leftKneeInfo.length));
          print("lKneeX: " + lKneeX.toString());
          print("lKneeY: " + lKneeY.toString());

          String leftHipInfo = inputData.toString().substring(
              inputData.toString().indexOf("leftHip") + 10,
              inputData.toString().indexOf("leftHip") + 52);
          print("lHipInfo: " + leftHipInfo);
          var lHipX = double.parse(leftHipInfo.substring(
              leftHipInfo.indexOf(":") + 1, leftHipInfo.indexOf(",")));
          var lHipY = double.parse(leftHipInfo.substring(
              leftHipInfo.indexOf(",") + 4, leftHipInfo.length));
          print("lHipX: " + lHipX.toString());
          print("lHipY: " + lHipY.toString());

          var calc2X = lHipX - lKneeX;
          if (calc2X < 0) {
            calc2X = calc2X * -1;
          }

          var calc2Y = lHipY - lKneeY;
          if (calc2Y < 0) {
            calc2Y = calc2Y * -1;
          }
          var angle2 = atan(calc2Y / calc2X);
          calculationOutput = calculationOutput + "Angle2(LHip and LKnee): " +
              angle2.toString() + " ";
          print("Angle Calculation 2: " + angle2.toString());
        }

        if (inputData.toString().contains("leftElbow")) {
          String leftElbowInfo = inputData.toString().substring(
              inputData.toString().indexOf("leftElbow") + 11,
              inputData.toString().indexOf("leftElbow") + 53);
          print("lElbowInfo: " + leftElbowInfo);
          var lElbowX = double.parse(leftElbowInfo.substring(
              leftElbowInfo.indexOf(":") + 1, leftElbowInfo.indexOf(",")));
          var lElbowY = double.parse(leftElbowInfo.substring(
              leftElbowInfo.indexOf(",") + 4, leftElbowInfo.length));
          print("lElbowX: " + lElbowX.toString());
          print("lElbowY: " + lElbowY.toString());

          String leftShoulderInfo = inputData.toString().substring(
              inputData.toString().indexOf("leftShoulder") + 14,
              inputData.toString().indexOf("leftShoulder") + 55);
          print("lShoulderInfo: " + leftShoulderInfo);
          var lShoulderX = double.parse(leftShoulderInfo.substring(
              leftShoulderInfo.indexOf(":") + 1,
              leftShoulderInfo.indexOf(",")));
          var lShoulderY = double.parse(leftShoulderInfo.substring(
              leftShoulderInfo.indexOf(",") + 4, leftShoulderInfo.length));
          print("lShoulderX: " + lShoulderX.toString());
          print("lShoulderY: " + lShoulderY.toString());

          var calc3X = lShoulderX - lElbowX;
          if (calc3X < 0) {
            calc3X = calc3X * -1;
          }

          var calc3Y = lShoulderY - lElbowY;
          if (calc3Y < 0) {
            calc3Y = calc3Y * -1;
          }
          var angle3 = atan(calc3Y / calc3X);
          calculationOutput =
              calculationOutput + "Angle3(LShoulder and LElbow): " +
                  angle3.toString() + " ";
          print("Angle Calculation 3: " + angle3.toString());
        }

        if (inputData.toString().contains("leftWrist")) {
          String leftElbowInfo = inputData.toString().substring(
              inputData.toString().indexOf("leftElbow") + 11,
              inputData.toString().indexOf("leftElbow") + 53);
          print("lElbowInfo: " + leftElbowInfo);
          var lElbowX = double.parse(leftElbowInfo.substring(
              leftElbowInfo.indexOf(":") + 1, leftElbowInfo.indexOf(",")));
          var lElbowY = double.parse(leftElbowInfo.substring(
              leftElbowInfo.indexOf(",") + 4, leftElbowInfo.length));
          print("lElbowX: " + lElbowX.toString());
          print("lElbowY: " + lElbowY.toString());

          String leftWristInfo = inputData.toString().substring(
              inputData.toString().indexOf("leftWrist") + 14,
              inputData.toString().indexOf("leftWrist") + 54);
          print("lWristInfo: " + leftWristInfo);
          var lWristX = double.parse(leftWristInfo.substring(
            12, leftWristInfo.indexOf(",")));
          var lWristY = double.parse(leftWristInfo.substring(
              leftWristInfo.indexOf(",") + 4, leftWristInfo.length));
          print("lWristX: " + lWristX.toString());
          print("lWristY: " + lWristY.toString());

          var calc4X = lWristX - lElbowX;
          if (calc4X < 0) {
            calc4X = calc4X * -1;
          }

          var calc4Y = lWristY - lElbowY;
          if (calc4Y < 0) {
            calc4Y = calc4Y * -1;
          }
          var angle4 = atan(calc4Y / calc4X);
          calculationOutput =
              calculationOutput + "Angle4(LWrist and LElbow): " +
                  angle4.toString() + " ";
          print("Angle Calculation 4: " + angle4.toString());
        }

        if(inputData.toString().contains("rightWrist")) {
          String rightElbowInfo = inputData.toString().substring(
              inputData.toString().indexOf("rightElbow") + 12,
              inputData.toString().indexOf("rightElbow") + 54);
          print("rElbowInfo: " + rightElbowInfo);
          var rElbowX = double.parse(rightElbowInfo.substring(
              rightElbowInfo.indexOf(":") + 1, rightElbowInfo.indexOf(",")));
          var rElbowY = double.parse(rightElbowInfo.substring(
              rightElbowInfo.indexOf(",") + 4, rightElbowInfo.length));
          print("rElbowX: " + rElbowX.toString());
          print("rElbowY: " + rElbowY.toString());

          String rightWristInfo = inputData.toString().substring(
              inputData.toString().indexOf("rightWrist") + 12,
              inputData.toString().indexOf("rightWrist") + 54);
          print("rWristInfo: " + rightWristInfo);
          var rWristX = double.parse(rightWristInfo.substring(
              rightWristInfo.indexOf(":") + 1, rightWristInfo.indexOf(",")));
          var rWristY = double.parse(rightWristInfo.substring(
              rightWristInfo.indexOf(",") + 4, rightWristInfo.length));
          print("rWristX: " + rWristX.toString());
          print("rWristY: " + rWristY.toString());

          var calc5X = rWristX - rElbowX;
          if (calc5X < 0) {
            calc5X = calc5X * -1;
          }

          var calc5Y = rWristY - rElbowY;
          if (calc5Y < 0) {
            calc5Y = calc5Y * -1;
          }
          var angle5 = atan(calc5Y / calc5X);
          calculationOutput = calculationOutput + "Angle5(RWrist and RElbow): " + angle5.toString() + " ";
          print("Angle Calculation 5: " + angle5.toString());
        }

        if(inputData.toString().contains("rightElbow")) {
          String rightElbowInfo = inputData.toString().substring(
              inputData.toString().indexOf("rightElbow") + 12,
              inputData.toString().indexOf("rightElbow") + 55);
          print("rElbowInfo: " + rightElbowInfo);
          var rElbowX = double.parse(rightElbowInfo.substring(
              rightElbowInfo.indexOf(":") + 1, rightElbowInfo.indexOf(",")));
          var rElbowY = double.parse(rightElbowInfo.substring(
              rightElbowInfo.indexOf(",") + 4, rightElbowInfo.length));
          print("rElbowX: " + rElbowX.toString());
          print("rElbowY: " + rElbowY.toString());

          String rightShoulderInfo = inputData.toString().substring(
              inputData.toString().indexOf("rightShoulder") + 15,
              inputData.toString().indexOf("rightShoulder") + 56);
          print("rShoulderInfo: " + rightShoulderInfo);
          var rShoulderX = double.parse(rightShoulderInfo.substring(
              rightShoulderInfo.indexOf(":") + 1, rightShoulderInfo.indexOf(",")));
          var rShoulderY = double.parse(rightShoulderInfo.substring(
              rightShoulderInfo.indexOf(",") + 4, rightShoulderInfo.length));
          print("rShoulderX: " + rShoulderX.toString());
          print("rShoulderY: " + rShoulderY.toString());

          var calc6X = rShoulderX - rElbowX;
          if (calc6X < 0) {
            calc6X = calc6X * -1;
          }

          var calc6Y = rShoulderY - rElbowY;
          if (calc6Y < 0) {
            calc6Y = calc6Y * -1;
          }
          var angle6 = atan(calc6Y / calc6X);
          calculationOutput = calculationOutput + "Angle6(RShoulder and RElbow): " + angle6.toString() + " ";
          print("Angle Calculation 6: " + angle6.toString());
        }

        if(inputData.toString().contains("rightHip")) {
          String rightHipInfo = inputData.toString().substring(
              inputData.toString().indexOf("rightHip") + 10,
              inputData.toString().indexOf("rightHip") + 53);
          print("rHipInfo: " + rightHipInfo);
          var rHipX = double.parse(rightHipInfo.substring(
              rightHipInfo.indexOf(":") + 1, rightHipInfo.indexOf(",")));
          var rHipY = double.parse(rightHipInfo.substring(
              rightHipInfo.indexOf(",") + 4, rightHipInfo.length));
          print("rHipX: " + rHipX.toString());
          print("rHipY: " + rHipY.toString());

          String rightShoulderInfo = inputData.toString().substring(
              inputData.toString().indexOf("rightShoulder") + 15,
              inputData.toString().indexOf("rightShoulder") + 56);
          print("rShoulderInfo: " + rightShoulderInfo);
          var rShoulderX = double.parse(rightShoulderInfo.substring(
              rightShoulderInfo.indexOf(":") + 1, rightShoulderInfo.indexOf(",")));
          var rShoulderY = double.parse(rightShoulderInfo.substring(
              rightShoulderInfo.indexOf(",") + 4, rightShoulderInfo.length));
          print("rShoulderX: " + rShoulderX.toString());
          print("rShoulderY: " + rShoulderY.toString());

          var calc7X = rShoulderX - rHipX;
          if (calc7X < 0) {
            calc7X = calc7X * -1;
          }

          var calc7Y = rShoulderY - rHipY;
          if (calc7Y < 0) {
            calc7Y = calc7Y * -1;
          }
          var angle7 = atan(calc7Y / calc7X);
          calculationOutput = calculationOutput + "Angle7(RShoulder and RHip): " + angle7.toString() + " ";
          print("Angle Calculation 7: " + angle7.toString());
        }

        if(inputData.toString().contains("leftHip")) {
          String leftHipInfo = inputData.toString().substring(
              inputData.toString().indexOf("leftHip") + 9,
              inputData.toString().indexOf("leftHip") + 52);
          print("lHipInfo: " + leftHipInfo);
          var lHipX = double.parse(leftHipInfo.substring(
              leftHipInfo.indexOf(":") + 1, leftHipInfo.indexOf(",")));
          var lHipY = double.parse(leftHipInfo.substring(
              leftHipInfo.indexOf(",") + 4, leftHipInfo.length));
          print("lHipX: " + lHipX.toString());
          print("lHipY: " + lHipY.toString());

          String leftShoulderInfo = inputData.toString().substring(
              inputData.toString().indexOf("leftShoulder") + 14,
              inputData.toString().indexOf("leftShoulder") + 55);
          print("lShoulderInfo: " + leftShoulderInfo);
          var lShoulderX = double.parse(leftShoulderInfo.substring(
              leftShoulderInfo.indexOf(":") + 1, leftShoulderInfo.indexOf(",")));
          var lShoulderY = double.parse(leftShoulderInfo.substring(
              leftShoulderInfo.indexOf(",") + 4, leftShoulderInfo.length));
          print("rShoulderX: " + lShoulderX.toString());
          print("rShoulderY: " + lShoulderY.toString());

          var calc8X = lShoulderX - lHipX;
          if (calc8X < 0) {
            calc8X = calc8X * -1;
          }

          var calc8Y = lShoulderY - lHipY;
          if (calc8Y < 0) {
            calc8Y = calc8Y * -1;
          }
          var angle8 = atan(calc8Y / calc8X);
          calculationOutput = calculationOutput + "Angle8(LShoulder and LHip): " + angle8.toString() + " ";
          print("Angle Calculation 8: " + angle8.toString());
        }


          //send info to airtable
        final response = await Dio().post(
          'https://api.airtable.com/v0/appbwQQuA7iPQKdB5/MoCap1',
          options: Options(
            contentType: 'Application/json',
            headers: {
              'Authorization': 'Bearer keyd27XwnU8As7tN9',
              'Accept': 'Application/json',
            },
          ),
          data: {

            'fields': {
              'Data': inputData.toString(),
              'AngleData': calculationOutput,
            }
          },
        );

        // TODO: Whatever you want to do with the response. A good practice is to transform it into models and than work with them
        print(response);
      } on DioError catch (e) {
        // TODO: Error handling
        if (e.response != null) {
          print("OOF: " + e.response.data);
        } else {
          print(e.request);
          print(e.message);
        }
    }
    }

      return true;



  }
  @override
  Widget build(BuildContext context) {
    if (controller == null || !controller.value.isInitialized) {
      return Container();
    }

    var tmp = MediaQuery.of(context).size;
    var screenH = math.max(tmp.height, tmp.width);
    var screenW = math.min(tmp.height, tmp.width);
    tmp = controller.value.previewSize;
    var previewH = math.max(tmp.height, tmp.width);
    var previewW = math.min(tmp.height, tmp.width);
    var screenRatio = screenH / screenW;
    var previewRatio = previewH / previewW;

    return OverflowBox(
      maxHeight:
          screenRatio > previewRatio ? screenH : screenW / previewW * previewH,
      maxWidth:
          screenRatio > previewRatio ? screenH / previewH * previewW : screenW,
      child: CameraPreview(controller),
    );
  }
}
