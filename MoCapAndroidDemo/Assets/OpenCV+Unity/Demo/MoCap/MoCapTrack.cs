﻿namespace OpenCvSharp.Demo
{
	using UnityEngine;
	using UnityEngine.EventSystems;

	using System;

	using OpenCvSharp;
	using OpenCvSharp.Tracking;

	public class MoCapTrack : WebCamera
	{
		WebCamDevice[] devices = WebCamTexture.devices;
		public String output = "";
		void Start()
		{

		}
		void OnGUI()
		{

		}
		protected override bool ProcessTexture(WebCamTexture input, ref Texture2D output)
		{
				foreach(WebCamDevice currentDevice in devices)
				{
					if (!currentDevice.isFrontFacing)
					{
						input.deviceName = currentDevice.name;
					}
				}
			for (int i = 0; i < devices.Length; i++)
			{
				output = output + devices[i].name + "  :  ";
			}
			input.Play();
			Mat image = Unity.TextureToMat(input, TextureParameters);

			output = Unity.MatToTexture(image, output);   //mask1 replaced the original   image
			return true;
		}
	}
}