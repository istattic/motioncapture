How can we "move the needle" for the project?

First, identify what needs to be done for the demo:
		1. Need rear camera working
		2. Need to track all colored markers ( and create bounding boxes so user can see )
		3. Need to add a button that starts and stops the logging of marker coordinates (and 			outputs to a log file that can be viewed in the app)



Second, identify what needs to be done in what order:
	Follow the numeric order above.


Third, give yourself deadlines for each step:
	1. 11/10
	2. 11/18
	3. 11/26