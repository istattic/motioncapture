﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;

public class StartCamScript : MonoBehaviour {

	public WebCamTexture webcam;

	// Use this for initialization
	void Start () {
		webcam = new WebCamTexture ();
		GameObject.Find ("DisplayCamera").GetComponentInChildren<MeshRenderer> ().material.mainTexture = webcam;
	}
	
	// Update is called once per frame
	void Update () {
		if (webcam.isPlaying) {
			Color32[] rawImg = webcam.GetPixels32 ();
			System.Array.Reverse (rawImg);
			processImage (rawImg, webcam.width, webcam.height);
			Debug.Log ("OK");
		}			
	}

	void OnMouseDown() {
		webcam.Play ();
	}

	[DllImport("CVTest", EntryPoint = "processImage")]
	public static extern void processImage(Color32[] raw, int width, int height);

}
