This is the guide for the motion capture ios application.

//Navigating to the correct part of the app after starting it

Upon launching the app, you'll see a still image. To navigate to the live feed capture, tap the video camera icon in the top right. To change from face detector to the pose detector tap on "Detectors" in the top right, then "Pose Detection" (not "Pose Detection, accurate") in the menu.

//Use

Start/Pause Button (bottom left of screen): Starts and stops the coordinate capture (turned off by default)

File Button (bottom right of screen): Opens the database

Camera Button (far top right of screen): Switches between front and rear camera feeds