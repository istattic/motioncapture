import numpy as np
from cv2 import cv2
#identifying input (right now it's a stock video)
cap = cv2.VideoCapture('examplefootage.webm')

ret, frame1 = cap.read()

#adjustable bounds to track differing colors (g=green  r=red  etc.)
g_l_r = np.array([10, 120, 0])
g_u_r = np.array([120, 255, 100])
r_l_r = np.array([0, 0, 160])
r_u_r = np.array([80, 50, 255])

while(cap.isOpened()):
    #create binary image where green is true
    greenMask = cv2.inRange(frame1, g_l_r, g_u_r)
    #identify contours in binary image
    gcontours, _ = cv2.findContours(greenMask, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    #draw boxes containing contours
    for contour in gcontours:
        (x, y, w, h) = cv2.boundingRect (contour)

        if cv2.contourArea(contour) < 700:
            continue
        cv2.rectangle(frame1, (x, y), (x+w, y+h), (0,255,0), 2)
    #process is repeated, but with red being true
    redMask = cv2.inRange(frame1, r_l_r, r_u_r)
    rcontours, _ = cv2.findContours(redMask, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

    for contour in rcontours:
        (x, y, w, h) = cv2.boundingRect (contour)

        if cv2.contourArea(contour) < 700:
            continue
        cv2.rectangle(frame1, (x, y), (x+w, y+h), (0,0,255), 2)

    #displaying video with overlay
    cv2.imshow('frame1', frame1)
    ret, frame1 = cap.read()
    #option for the user to exit the video early (before completion)
    if cv2.waitKey(15) & 0xFF == ord('q'):
        break
#closes program
cap.release()
cv2.destroyAllWindows()