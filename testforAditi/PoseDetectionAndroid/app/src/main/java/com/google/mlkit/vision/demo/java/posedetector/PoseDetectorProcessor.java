/*
 * Copyright 2020 Google LLC. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.google.mlkit.vision.demo.java.posedetector;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.os.SystemClock;
import android.telephony.TelephonyManager;
import android.util.JsonReader;
import android.util.Log;
import java.lang.Math.*;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import android.provider.Settings.Secure;
import com.google.android.gms.tasks.Task;
import com.google.mlkit.vision.common.InputImage;
import com.google.mlkit.vision.demo.GraphicOverlay;
import com.google.mlkit.vision.demo.java.posedetector.classification.PoseClassifierProcessor;
import com.google.mlkit.vision.pose.Pose;
import com.google.mlkit.vision.pose.PoseDetection;
import com.google.mlkit.vision.pose.PoseDetector;
import com.google.mlkit.vision.pose.PoseDetectorOptionsBase;
import com.google.mlkit.vision.pose.PoseLandmark;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.UUID;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import okhttp3.Call;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/** A processor to run pose detector. */
@RequiresApi(api = Build.VERSION_CODES.O)
public class PoseDetectorProcessor
    extends VisionProcessorBase<PoseDetectorProcessor.PoseWithClassification> {
  private static final String TAG = "PoseDetectorProcessor";

  private final PoseDetector detector;
  public static boolean toggled = false;
  private final boolean showInFrameLikelihood;
  private final boolean visualizeZ;
  private final boolean rescaleZForVisualization;
  private final boolean runClassification;
  private final boolean isStreamMode;
  private final Context context;
  private final Executor classificationExecutor;
  private final String startTime = LocalDateTime.now().toString();
  private final String uniqueID = UUID.randomUUID().toString();
  private PoseClassifierProcessor poseClassifierProcessor;

  /**
   * Internal class to hold Pose and classification results.
   */
  protected static class PoseWithClassification {
    private final Pose pose;
    private final List<String> classificationResult;

    public PoseWithClassification(Pose pose, List<String> classificationResult) {
      this.pose = pose;
      this.classificationResult = classificationResult;
    }

    public Pose getPose() {
      return pose;
    }

    public List<String> getClassificationResult() {
      return classificationResult;
    }
  }

  public PoseDetectorProcessor(
          Context context,
          PoseDetectorOptionsBase options,
          boolean showInFrameLikelihood,
          boolean visualizeZ,
          boolean rescaleZForVisualization,
          boolean runClassification,
          boolean isStreamMode) {
    super(context);
    this.showInFrameLikelihood = showInFrameLikelihood;
    this.visualizeZ = visualizeZ;
    this.rescaleZForVisualization = rescaleZForVisualization;
    detector = PoseDetection.getClient(options);
    this.runClassification = runClassification;
    this.isStreamMode = isStreamMode;
    this.context = context;
    classificationExecutor = Executors.newSingleThreadExecutor();
  }

  @Override
  public void stop() {
    super.stop();
    detector.close();
  }

  @Override
  protected Task<PoseWithClassification> detectInImage(InputImage image, int frameCount) {
    return detector
            .process(image)
            .continueWith(
                    classificationExecutor,
                    task -> {
                      Pose pose = task.getResult();
                      List<String> classificationResult = new ArrayList<>();
                      if (runClassification) {
                        if (poseClassifierProcessor == null) {
                          poseClassifierProcessor = new PoseClassifierProcessor(context, isStreamMode);
                        }
                        classificationResult = poseClassifierProcessor.getPoseResult(pose);
                      }
                      Log.d(TAG, "frameCount " + frameCount);
                      return new PoseWithClassification(pose, classificationResult);
                    });
  }

  @Override
  protected void onSuccess(
          @NonNull PoseWithClassification poseWithClassification,
          @NonNull GraphicOverlay graphicOverlay, String startMs, String endMs) {
    Log.d(TAG, "Pose" + poseWithClassification.pose);
    graphicOverlay.add(
            new PoseGraphic(
                    graphicOverlay, poseWithClassification.pose, showInFrameLikelihood, visualizeZ,
                    rescaleZForVisualization, poseWithClassification.classificationResult));

    Pose pose = poseWithClassification.pose;
    List<PoseLandmark> landmarks = pose.getAllPoseLandmarks();
    if (landmarks.isEmpty()) {
      return;
    }

    PoseLandmark leftShoulder = pose.getPoseLandmark(PoseLandmark.LEFT_SHOULDER);
    Log.d(TAG, "leftShoulder" + leftShoulder.getPosition3D());
    PoseLandmark rightShoulder = pose.getPoseLandmark(PoseLandmark.RIGHT_SHOULDER);
    PoseLandmark leftElbow = pose.getPoseLandmark(PoseLandmark.LEFT_ELBOW);
    PoseLandmark rightElbow = pose.getPoseLandmark(PoseLandmark.RIGHT_ELBOW);
    PoseLandmark leftWrist = pose.getPoseLandmark(PoseLandmark.LEFT_WRIST);
    PoseLandmark rightWrist = pose.getPoseLandmark(PoseLandmark.RIGHT_WRIST);
    PoseLandmark leftHip = pose.getPoseLandmark(PoseLandmark.LEFT_HIP);
    PoseLandmark rightHip = pose.getPoseLandmark(PoseLandmark.RIGHT_HIP);
    PoseLandmark leftKnee = pose.getPoseLandmark(PoseLandmark.LEFT_KNEE);
    PoseLandmark rightKnee = pose.getPoseLandmark(PoseLandmark.RIGHT_KNEE);
    PoseLandmark leftAnkle = pose.getPoseLandmark(PoseLandmark.LEFT_ANKLE);
    PoseLandmark rightAnkle = pose.getPoseLandmark(PoseLandmark.RIGHT_ANKLE);

    PoseLandmark leftPinky = pose.getPoseLandmark(PoseLandmark.LEFT_PINKY);
    PoseLandmark rightPinky = pose.getPoseLandmark(PoseLandmark.RIGHT_PINKY);
    PoseLandmark leftIndex = pose.getPoseLandmark(PoseLandmark.LEFT_INDEX);
    PoseLandmark rightIndex = pose.getPoseLandmark(PoseLandmark.RIGHT_INDEX);
    PoseLandmark leftThumb = pose.getPoseLandmark(PoseLandmark.LEFT_THUMB);
    PoseLandmark rightThumb = pose.getPoseLandmark(PoseLandmark.RIGHT_THUMB);
    PoseLandmark leftHeel = pose.getPoseLandmark(PoseLandmark.LEFT_HEEL);
    PoseLandmark rightHeel = pose.getPoseLandmark(PoseLandmark.RIGHT_HEEL);
    PoseLandmark leftFootIndex = pose.getPoseLandmark(PoseLandmark.LEFT_FOOT_INDEX);
    PoseLandmark rightFootIndex = pose.getPoseLandmark(PoseLandmark.RIGHT_FOOT_INDEX);
    //OkHttpClient client = new OkHttpClient();
    String data = "Left Shoulder: " + leftShoulder.getPosition3D() +
            "Right Shoulder: " + rightShoulder.getPosition3D() +
            "Left Elbow: " + leftElbow.getPosition3D() +
            "Right Elbow: " + rightElbow.getPosition3D() +
            "Left Wrist: " + leftWrist.getPosition3D() +
            "Right Wrist: " + rightWrist.getPosition3D() +
            "Left Hip: " + leftHip.getPosition3D() +
            "Right Hip: " + rightHip.getPosition3D() +
            "Left Knee: " + leftKnee.getPosition3D() +
            "Right Knee: " + rightKnee.getPosition3D() +
            "Left Ankle: " + leftAnkle.getPosition3D() +
            "Right Ankle: " + rightAnkle.getPosition3D() +
            "Left Pinky: " + leftPinky.getPosition3D() +
            "Right Pinky: " + rightPinky.getPosition3D() +
            "Left Index: " + leftIndex.getPosition3D() +
            "Right Index: " + rightIndex.getPosition3D() +
            "Left Thumb: " + leftThumb.getPosition3D() +
            "Right Thumb: " + rightThumb.getPosition3D() +
            "Left Heel: " + leftHeel.getPosition3D() +
            "Right Heel: " + rightHeel.getPosition3D() +
            "Left FootIndex: " + leftFootIndex.getPosition3D() +
            "Right FootIndex: " + rightFootIndex.getPosition3D();
    String currentTime = LocalDateTime.now().toString();

    //angle calculations
    String calculationOutput = "";
    float calc1X = rightHip.getPosition3D().getX() - rightKnee.getPosition3D().getX();
    float calc1Y = rightHip.getPosition3D().getY() - rightKnee.getPosition3D().getY();
    double angle1 = Math.atan(calc1Y / calc1X);
    calculationOutput = calculationOutput + "Angle1(RHip and RKnee): " +
            angle1 + " ";
    float calc2X = leftHip.getPosition3D().getX() - leftKnee.getPosition3D().getX();
    float calc2Y = leftHip.getPosition3D().getY() - leftKnee.getPosition3D().getY();
    double angle2 = Math.atan(calc2Y / calc2X);
    calculationOutput = calculationOutput + "Angle2(LHip and LKnee): " +
            angle2 + " ";

    float calc3X = leftKnee.getPosition3D().getX() - leftAnkle.getPosition3D().getX();
    float calc3Y = leftKnee.getPosition3D().getY() - leftAnkle.getPosition3D().getY();
    double angle3 = Math.atan(calc3Y / calc3X);
    calculationOutput = calculationOutput + "Angle3(LAnkle and LKnee): " +
            angle3 + " ";

    float calc4X = rightKnee.getPosition3D().getX() - rightAnkle.getPosition3D().getX();
    float calc4Y = rightKnee.getPosition3D().getY() - rightAnkle.getPosition3D().getY();
    double angle4 = Math.atan(calc4Y / calc4X);
    calculationOutput = calculationOutput + "Angle4(RAnkle and RKnee): " +
            angle4 + " ";
    float calc5X = leftShoulder.getPosition3D().getX() - leftHip.getPosition3D().getX();
    float calc5Y = leftShoulder.getPosition3D().getY() - leftHip.getPosition3D().getY();
    double angle5 = Math.atan(calc5Y / calc5X);
    calculationOutput = calculationOutput + "Angle5(LHip and LShoulder): " +
            angle5 + " ";
    float calc6X = rightShoulder.getPosition3D().getX() - rightHip.getPosition3D().getX();
    float calc6Y = rightShoulder.getPosition3D().getY() - rightHip.getPosition3D().getY();
    double angle6 = Math.atan(calc6Y / calc6X);
    calculationOutput = calculationOutput + "Angle6(RHip and RShoulder): " +
            angle6 + " ";
    float calc7X = leftShoulder.getPosition3D().getX() - leftElbow.getPosition3D().getX();
    float calc7Y = leftShoulder.getPosition3D().getY() - leftElbow.getPosition3D().getY();
    double angle7 = Math.atan(calc7Y / calc7X);
    calculationOutput = calculationOutput + "Angle7(LElbow and LShoulder): " +
            angle7 + " ";
    float calc8X = rightShoulder.getPosition3D().getX() - rightElbow.getPosition3D().getX();
    float calc8Y = rightShoulder.getPosition3D().getY() - rightElbow.getPosition3D().getY();
    double angle8 = Math.atan(calc8Y / calc8X);
    calculationOutput = calculationOutput + "Angle8(RElbow and RShoulder): " +
            angle8 + " ";
    float calc9X = leftWrist.getPosition3D().getX() - leftElbow.getPosition3D().getX();
    float calc9Y = leftWrist.getPosition3D().getY() - leftElbow.getPosition3D().getY();
    double angle9 = Math.atan(calc9Y / calc9X);
    calculationOutput = calculationOutput + "Angle9(LElbow and LWrist): " +
            angle9 + " ";
    float calc10X = rightWrist.getPosition3D().getX() - rightElbow.getPosition3D().getX();
    float calc10Y = rightWrist.getPosition3D().getY() - rightElbow.getPosition3D().getY();
    double angle10 = Math.atan(calc10Y / calc10X);
    calculationOutput = calculationOutput + "Angle9(RElbow and RWrist): " +
            angle10 + " ";
    //

      MediaType JSON = MediaType.parse("application/json; charset=utf-8");
      JSONObject jsonObject = new JSONObject();
      JSONObject jsonPoseData = new JSONObject();

      try {
        jsonPoseData.put("Name", uniqueID);
        jsonPoseData.put("Data", data);
        jsonPoseData.put("AngleData", calculationOutput);
        jsonPoseData.put("RunStartTime", startTime);
        jsonPoseData.put("Time", currentTime);
        jsonPoseData.put("Platform", "android");
      } catch (JSONException e) {
        e.printStackTrace();
      }
      try {
        jsonObject.put("fields", jsonPoseData);
      } catch (Exception e) {
        e.printStackTrace();
      }
      RequestBody body = RequestBody.create(JSON, jsonObject.toString());
      OkHttpClient client = new OkHttpClient();

      Runnable runnable = new Runnable() {
        @Override
        public void run() {
          Request request = new Request.Builder()
                  .url("https://api.airtable.com/v0/appVaU6XUyQmFc2Nl/table1?api_key=keyd27XwnU8As7tN9")
                  .post(body)
                  .build();

          try {
            if (toggled) {
              Response response = client.newCall(request).execute();
              String jsonBody = response.body().string();
            }
          } catch (Exception exception) {
          }
        }
      };
      new Thread(runnable).start();
    }

    @Override
    protected void onFailure (@NonNull Exception e){
      Log.e(TAG, "Pose detection failed!", e);
    }
  }
